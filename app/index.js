/**
 * Application entry point
 */

// Load application styles

// ================================
// START YOUR APP HERE
// ================================
var autocomplete, directionsDisplay, directionsService;

window.initAutocomplete = function() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('bt-drive-form__from')),
        {types: ['geocode']});
}


window.findAddress = function(){
    var address = document.getElementById('bt-drive-form__from').value,
        travelMode = document.querySelector('[name=drive\\[transport\\]]:checked').value;        
    
    directionsService.route({
      origin: "Galeria Bałtycka, Gdańsk",
      destination: address,
      travelMode: travelMode
    }, function(response, status) {
      if (status === 'OK') {
        directionsDisplay.setDirections(response);
      } else {
        //window.alert('Directions request failed due to ' + status);
      }
    });
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
window.geolocate = function() {
if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
    var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
    };
    var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
    });
    autocomplete.setBounds(circle.getBounds());
    });
}
}

//Google maps API initialisation
window.onLoad = function() {
    
    return (function(window, google){
        window.geocoder = new google.maps.Geocoder();  
        directionsService = new google.maps.DirectionsService;
        directionsDisplay = new google.maps.DirectionsRenderer;

        window.map = new google.maps.Map(document.getElementById('bt-drive__map'), {
            center: new google.maps.LatLng(57, 21),
            zoom: 3,
            mapTypeId: "OSM",
            mapTypeControl: false,
            streetViewControl: false
        });
        directionsDisplay.setMap(map);
        
        //Define OSM map type pointing at the OpenStreetMap tile server
        map.mapTypes.set("OSM", new google.maps.ImageMapType({
            getTileUrl: function(coord, zoom) {
                // "Wrap" x (logitude) at 180th meridian properly
                // NB: Don't touch coord.x because coord param is by reference, and changing its x property breakes something in Google's lib 
                var tilesPerGlobe = 1 << zoom;
                var x = coord.x % tilesPerGlobe;
                if (x < 0) {
                    x = tilesPerGlobe+x;
                }
                // Wrap y (latitude) in a like manner if you want to enable vertical infinite scroll

                return "http://tile.openstreetmap.org/" + zoom + "/" + x + "/" + coord.y + ".png";
            },
            tileSize: new google.maps.Size(256, 256),
            name: "OpenStreetMap",
            maxZoom: 18
        }));
    })(window, google);
}