const path = require('path');

const Encore = require('@symfony/webpack-encore'),
    webpack = require('webpack'),
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    CopyWebpackPlugin = require('copy-webpack-plugin');
    

Encore
    // directory where all compiled assets will be stored
    .setOutputPath('./dist')

    // what's the public path to this directory (relative to your project's document root dir)
    .setPublicPath('/')

    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()

    // JS
    .addEntry('assets/js/app', './app/index.js')

    .addEntry('assets/js/vendor', './app/vendor.js')

    // Stylesheet
    .addStyleEntry('assets/css/global', './app/assets/styles/index.scss')

    // allow sass/scss files to be processed
    .enableSassLoader()

    .addPlugin(new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
        Popper: ['popper.js', 'default'],
        Waves: 'node-waves' 
    }))

    // Add ejs loader and plugin depends
    .addLoader({ test: /\.ejs$/, loader: 'ejs-render-loader' })    
    
    .addPlugin(new webpack.ProvidePlugin({
        // lodash
        '_': 'lodash'
    }))

    .addPlugin(new HtmlWebpackPlugin({
        template: './app/index.ejs'
    }))

    .enableSourceMaps(!Encore.isProduction())

    .configureBabel(function(babelConfig) {
        babelConfig.presets.push('es2015');
    })

    .addPlugin(new CopyWebpackPlugin([
        { 
            from: 'static',
            to: 'static'
        }
    ]));


    // create hashed filenames (e.g. app.abc123.css)
    // .enableVersioning()
;

let config = Encore.getWebpackConfig();

// export the final configuration
module.exports = config;